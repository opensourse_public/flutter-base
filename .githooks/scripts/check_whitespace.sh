#!/bin/bash
# A git hook script to find and fix trailing whitespace
# in your commits. Bypass it with the --no-verify option
# to git-commit

if git rev-parse --verify HEAD >/dev/null 2>&1
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

# Print offending trailing whitespaces
git diff-index --check --cached $against --

# change IFS to ignore filename's space in |for|
IFS="
"
# autoremove trailing whitespace
for line in `git diff --check --cached | sed '/^[+-]/d'` ; do
    # get file name
    file="`echo $line | sed -r 's/:[0-9]+: .*//'`"

    # display tips
    echo -e "auto-removing whitespaces: \033[31m$file\033[0m!"

    # since $file in working directory isn't always equal to $file in index, so we backup it
    mv -f "$file" "${file}.save"

    # discard changes in working directory
    git checkout -- "$file"

    # remove trailing whitespace and trailing newline at EOF
    sed -i -e 's/[[:space:]]*$//' -e :a -e '/^\n*$/{$d;N;};/\n$/ba' "$file"
    git add "$file"

    # restore the $file
    sed -e 's/[[:space:]]*$//' -e :a -e '/^\n*$/{$d;N;};/\n$/ba' "${file}.save" > "$file"
    rm "${file}.save"
done

# Recheck whitespaces after autoremove, eventually print them
exec git diff-index --check --cached $against --
