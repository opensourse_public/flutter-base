# Flutter base

[//]: # "INTRO (this is a comment)"
A cross platform mobile app written in Dart/Flutter

## Feature overview

*   [x] **Folder structure**
*   [x] **Logging** that's customizable
*   [x] **Tests** with coverage
*   [ ] **Visuals** to keep users engaged

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Usage](#usage)
    *   [Workflow](#workflow)
*   [Details](#details)
    * [Folder structure](#folder-structure)
    * [Logging](#logging)
* [Tests](#tests)
    * [Static tests (Linting)](#static-tests-linting)
    * [Dynamic tests (Unit, Widget, Integration)](#dynamic-tests-unit-widget-integration)
    * [Mocks](#mocks)
*   [Contribute](#contribute)
*   [License](#license)
*   [Sources](#sources)
*   [Conclusion](#conclusion)

## What is this?

This project is a feature rich Flutter template that you can customize to your needs.
You can add features you need in your particular case.

## Why should I use this?

Anybody is starting somewhere, so did I. Not knowing what the project architecture should be
nor the folder structure. Do not Google all those questions again. I did it for you already.

## Getting Started

The best way to get started with this project is by using AndroidStudio IDE. As this is a
Flutter/Dart project, follow the [Flutter docs](https://flutter.dev/docs) to set up your developer environment.

### Requirements

* Have a project ready where you can add a README
* Basic knowledge of [Markdown][about-markdown] (here is a [Cheatsheet][markdown-cheatsheet])

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/readme-template
```

### Usage

Create a configuration file `/assets/cfg/config.json` needed to build the app:

```json
 {
    "appName": "My App",

    "logLevel": "debug",
    "logColor": true,
    "logEmoji": false,

    "devicePreview": false
 }
```

### Workflow

Follow this checklist if you want to add something to the code:

**Create new branch**
- derived from `dev` branch
- descriptive name of the feature

**Add commits**
- each commit has to focus on one type of changes
- describe intent of the commit and add links in a commit message
- `flutter test` and `flutter analyze` have to pass
- think again about your changes and refactor if possible

**Merge request**
- after the branch is finished fill in a merge request


## Details

A more detailed description of the project's source code

### Folder structure

Flutter projects contain 3 main folders: `/android`, `/ios`, `/lib`. The first two contain platform specific
code which is needed to run the common Dart/Flutter code from `/lib` folder. Let's break down this folder:

- `common` - widgets shared across multiple routes
    - `wrappers` - wrap a equally named Flutter widget with functionality we want to use across the whole app
- `global` - global states that can be listened to anywhere in the app (using ChangeNotifier)
- `models` - entities used in the app, often implement transformation between JSON and Objects
- `pages` - "pages" of the app, higher level widgets containing subwidgets
- `utils` - helper classes

### Logging

For logging events we are using [logger package](https://pub.dev/packages/logger). We customized how
the logs look like by editing the [SimplePrinter](https://github.com/leisim/logger/blob/master/lib/src/printers/simple_printer.dart).
This is how we use the different log levels:

- **verbose** - finest details, data processing (requests, json parsing, ..), USE in Streams
- **debug** - more detailed info about processes which could help locate some bugs
- **info** - captures user interactions and general progress in the app flow
- **warning** - potential harmful situations that indicate potential problems
- **error** - error situations that might still allow the app to continue running
- **wtf** - fatal failures that might cause the application to terminate

## Tests

### Static tests (Linting)

We set up linting rules in this project to ensure code quality and a common coding style. We are
using the [lint package](https://pub.dev/packages/lint) where you can find all the [rules](https://github.com/passsy/dart-lint/blob/master/lib/analysis_options.yaml)
which are enabled. The package follows [Effective Dart](https://dart.dev/guides/language/effective-dart)
where you can learn more about how to use Dart the right way.

The linting configuration can be found in `analysis_options.yaml` file. We leave the lint package rules almost
unchanged, we just add stricter severity to them. During CI/CD all severities are treated as errors.
You can therefore compile code with **warnings**  or **hints** but the final production code has to be without
any rule violations!

`flutter analyze` runs the analyzer/linter and checks the code for lint rules violations

### Dynamic tests (Unit, Widget, Integration)

In a Flutter project there are 3 basic types of dynamic tests. Unit and Widget tests can be found in
the `/test` folder. Integration tests in the `/test_driver` folder. Use the link to learn more about
[Flutter testing](https://flutter.dev/docs/testing).

Some widgets need to behave differently during testing. Typically Widgets performing some network
communication (fails in tests). To get exposure to this information inside Widgets, use
`Test.of(context).isTested`

- `flutter test` runs unit and widget tests on the PC (no device needed)
- `flutter run [pathToTestFile]` runs widget test on a connected device, good for debugging tests
- `flutter drive --target=test_driver/app.dart` runs integration tests on a connected device
- `./coverage.sh` runs the test coverage of Unit and Widget tests and generates a report using LCOV

### Mocks

We are using the [mockito package](https://pub.dev/packages/mockito) for mocking objects. Define the
mocks you want to generate in `test/mocks/mockito.dart`, then generate them by running:

`flutter pub run build_runner build`

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources

[react-markdown][react-markdown] - Project which served as an inspiration for this README

[//]: # "Source definitions"
[react-markdown]: https://github.com/remarkjs/react-markdown "React-markdown project"

## Conclusion

To summarize..

We have a feature base Flutter example project from which we can build our project faster and easier.
