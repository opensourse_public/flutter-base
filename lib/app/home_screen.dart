import 'package:flutter/material.dart';
import 'package:flutter_base/features/counter/widgets/counter.dart';
import 'package:flutter_base/features/posts/models/post_model.dart';
import 'package:flutter_base/models/example_model.dart';
import 'package:flutter_base/repositories/repos.dart';
import 'package:flutter_base/repositories/test.dart';
import 'package:flutter_base/widgets/my_futurebuilder.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bool isTested = Test.of(context)!.isTested;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: Column(
          children: [
            const Text('Content'),
            Text('isTested = $isTested'),
            Consumer<Repos>(
              builder: (_, services, __) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(32.0),
                      child: Counter(),
                    ),
                    MyFutureBuilder<ExampleModel>(
                      future: services.external.fetchDummy(),
                      builder: (exampleModel) => Text(
                        'externalService.fetchDummy() = ${exampleModel.name}',
                      ),
                    ),
                    const SizedBox(height: 16),
                    MyFutureBuilder<List<PostModel>>(
                      future: services.posts.fetchPosts(),
                      builder: (exampleModel) => Text(
                        'number of fetched posts = ${exampleModel.length}',
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}
