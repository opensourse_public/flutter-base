import 'package:flutter/material.dart';
import 'package:flutter_base/app/home_screen.dart';
import 'package:flutter_base/utils/logger.dart';

class RouteGenerator {
  static final log = getLogger(RouteGenerator);

  static Route<dynamic> generate(RouteSettings settings) {
    final name = settings.name;
    //final args = settings.arguments;

    log.i('Opening new route "$name"');
    switch (name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      default:
        final String message = 'Undefined route $name';
        log.f(message);
        throw message;
    }
  }
}
