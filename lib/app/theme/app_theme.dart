import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/app/theme/dark_colorscheme.dart';
import 'package:flutter_base/app/theme/light_colorscheme.dart';
import 'package:flutter_base/app/theme/text_theme.dart';

class AppTheme {
  static const transparent = Colors.transparent;

  /// Shades of GREY
  /// prefer picking the closest possible, instead of defining a new color
  // static const white = Colors.white;                  <-- USE `context.colorScheme.onPrimary` INSTEAD
  // static Color grey50 = Colors.grey.shade50;          <-- USE `context.colorScheme.surfaceVariant` INSTEAD
  // static Color grey200 = Colors.grey.shade200;        <-- USE `context.colorScheme.outlineVariant` INSTEAD
  // static Color grey900 = Colors.grey.shade900;        <-- USE `context.colorScheme.outline` INSTEAD

  /// Black and White
  static const black = Colors.black;
  static const white = Colors.white;

  /// Shades of RED -> Error
  /// prefer picking the closest possible, instead of defining a new color
  // static const error = Colors.red;                    <-- USE `context.colorScheme.error` INSTEAD
  // static final errorContainer = Colors.red.shade50;   <-- USE `context.colorScheme.errorContainer` INSTEAD

  /// Shades of GREEN -> Success
  /// prefer picking the closest possible, instead of defining a new color
  static const success = Colors.green;
  static final successContainer = success.withAlpha(34);

  /// Shades of ORANGE -> Warning
  /// prefer picking the closest possible, instead of defining a new color
  static const warning = Colors.orange;
  static final warningContainer = warning.withAlpha(34);

  /// Shades of BLUE -> Info
  /// prefer picking the closest possible, instead of defining a new color
  static const info = Colors.lightBlue;
  static final infoContainer = info.withAlpha(34);

  /// Other colors
  // static const purple = Colors.purple;

  /// THEME
  static final ThemeData _lightBase = _base(Brightness.light);
  static final ThemeData appThemeLight = _applyCommonSettings(_lightBase).copyWith();

  static final ThemeData _darkBase = _base(Brightness.dark);
  static final ThemeData appThemeDark = _applyCommonSettings(_darkBase).copyWith();

  static ThemeData _applyCommonSettings(ThemeData theme) => theme.copyWith(
        appBarTheme:
            AppBarTheme(foregroundColor: theme.colorScheme.onPrimary, systemOverlayStyle: SystemUiOverlayStyle.light),
        dividerTheme: const DividerThemeData(
          space: 0,
        ),
        switchTheme: SwitchThemeData(
          thumbColor: MaterialStateProperty.all(theme.colorScheme.onPrimary),
          trackColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
            if (states.contains(MaterialState.selected)) {
              return theme.colorScheme.primary;
            }
            return theme.colorScheme.outlineVariant;
          }),
        ),
        radioTheme: const RadioThemeData(
          visualDensity: VisualDensity.compact,
        ),
        checkboxTheme: CheckboxThemeData(
          shape: const CircleBorder(),
          side: BorderSide(color: theme.colorScheme.outline),
        ),
        cardTheme: const CardTheme(elevation: 1),
        bottomNavigationBarTheme: theme.bottomNavigationBarTheme.copyWith(
          //elevation: 24, custom shadow needed to be added
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedItemColor: theme.colorScheme.primary,
          selectedIconTheme: theme.iconTheme.copyWith(
            color: theme.colorScheme.primary,
            fill: 1.0,
          ),
          selectedLabelStyle: theme.textTheme.bodyMedium,
          unselectedLabelStyle: theme.textTheme.bodyMedium,
        ),
        filledButtonTheme: FilledButtonThemeData(
          style: FilledButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: theme.colorScheme.primary,
          foregroundColor: theme.colorScheme.onPrimary,
          // shape: const CircleBorder(),
        ),
        snackBarTheme: SnackBarThemeData(
          contentTextStyle: textTheme.labelLarge,
          backgroundColor: theme.colorScheme.outline,
        ),
      );

  static ThemeData _base(Brightness brightness) {
    final ColorScheme colorScheme = brightness == Brightness.light ? flexSchemeLight : flexSchemeDark;

    return ThemeData.from(
      useMaterial3: true,
      textTheme: textTheme,
      colorScheme: colorScheme,
    );
  }
}
