import 'package:flutter/material.dart';
import 'package:flutter_base/features/counter/state/counter_provider.dart';
import 'package:provider/provider.dart';

class Counter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<CounterProvider>(
      builder: (_, manager, __) {
        return Column(
          children: [
            Text('Global counter: ${manager.count}'),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: manager.decrement,
                  child: const Text('-'),
                ),
                TextButton(
                  onPressed: manager.increment,
                  child: const Text('+'),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
