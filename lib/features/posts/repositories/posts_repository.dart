import 'package:flutter_base/features/posts/models/post_model.dart';
import 'package:flutter_base/repositories/controllers/base_controller.dart';

abstract class PostsRepository {
  Future<List<PostModel>> fetchPosts();
}

class PostsRepositoryImpl extends BaseController implements PostsRepository {
  PostsRepositoryImpl({required super.baseUrl});

  @override
  Future<List<PostModel>> fetchPosts() async {
    final items = await getJsonList('/posts');
    return items.map((item) => PostModel.fromJson(item as Map<String, dynamic>)).toList();
  }
}
