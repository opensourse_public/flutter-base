import 'package:flutter/material.dart';
import 'package:flutter_base/app/app.dart';
import 'package:flutter_base/app/app_wrapper.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

/// Code run during splash screen
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: 'assets/cfg/.env');
  runApp(AppWrapper(app: App()));
}
