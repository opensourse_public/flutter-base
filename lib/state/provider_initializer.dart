import 'package:flutter/material.dart';
import 'package:flutter_base/features/counter/state/counter_provider.dart';
import 'package:flutter_base/features/posts/repositories/posts_repository.dart';
import 'package:flutter_base/repositories/database/external_repository.dart';
import 'package:flutter_base/repositories/repos.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

class ProviderInitializer extends StatelessWidget {
  final String apiUrl = dotenv.get('API_URL', fallback: '');

  final log = getLogger(ProviderInitializer);
  final Widget child;

  ProviderInitializer({required this.child});

  @override
  Widget build(BuildContext context) {
    log.i('Initializing ChangeNotifiers');
    final exampleManager = CounterProviderImpl(count: 100);

    log.i('Initializing Services');
    final externalRepo = ExternalRepositoryImpl();
    final postsRepo = PostsRepositoryImpl(baseUrl: apiUrl);
    final services = ReposImpl(
      external: externalRepo,
      posts: postsRepo,
    );

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<Repos>(create: (_) => services),
        ChangeNotifierProvider<CounterProvider>(create: (_) => exampleManager),
      ],
      child: child,
    );
  }
}
